# DOS RT
Startup code and minimal runtime for DOS

## Idea
LLVM cannot create 16 bit code, so the idea is to enable DOS programs to be written in rust by using a shim written in 16 bit assembly to set the CPU into protected mode before exectuing code created by LLVM. The DPMI interface can be used to do this in a somewhat compliant interface.

## Research Resources:
 - DPMI Overview https://en.wikipedia.org/wiki/DOS_Protected_Mode_Services
 - DPMI Spec http://www.delorie.com/djgpp/doc/dpmi/
 - Virtual 8086 https://wiki.osdev.org/Virtual_8086_Mode
 - DOS Extender example (with source) http://www.sid6581.net/pmodew/
 - 8086 Registers https://en.wikipedia.org/wiki/Intel_8086#Details
 - djgpp - gnu on dos utilising a flat address space http://www.delorie.com/djgpp/ & https://en.wikipedia.org/wiki/DJGPP
 - discussion of memory models in dos https://news.ycombinator.com/item?id=24100361
 - DOS int 21 services API https://www.beck-ipc.com/api_files/scxxx/dosemu.htm
 - More on int 21 http://www.hermit.cc/it/dos/config.htm
 - DOS Executable format https://en.wikipedia.org/wiki/DOS_MZ_executable
 -
 - info on setting up an rt crate https://rust-embedded.github.io/embedonomicon/ & https://rust-embedded.github.io/book/intro/no-std.html
 - 
 - Serrentty's attempt at this idea https://github.com/Serentty/rusty-dos
 - Someone on reddit thinking about it https://www.reddit.com/r/rust/comments/ask2v5/dos_the_final_frontier/ & https://www.reddit.com/r/rust/comments/ane10g/creating_a_dos_executable_with_rust/

## Reason for Archival:
Research indicates project is technically feasible, however work required vastly outstrips demand for this idea.

**Conclusion: Dig it up if someone actually needs it.**
